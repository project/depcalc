<?php

/**
 * @file
 * Drupal Module: Dependency Calculation.
 *
 * Calculates recursive dependencies of any entity.
 */

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\depcalc\DependencyCalculatorEvents;
use Drupal\depcalc\Event\InvalidateDepcalcCacheEvent;

/**
 * Implements hook_entity_presave().
 */
function depcalc_entity_presave(EntityInterface $entity) {
  if ($entity->uuid()) {
    $event = new InvalidateDepcalcCacheEvent($entity);
    /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher */
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event_dispatcher->dispatch($event, DependencyCalculatorEvents::INVALIDATE_DEPCALC_CACHE);
  }
}

/**
 * Implements hook_entity_delete().
 */
function depcalc_entity_delete(EntityInterface $entity) {
  if ($uuid = $entity->uuid()) {
    $backend = \Drupal::cache('depcalc');
    $backend->delete($uuid);
    Cache::invalidateTags([$uuid]);
  }
}
