<?php

namespace Drupal\Tests\depcalc\Kernel;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\depcalc\DependentEntityWrapper;
use Drupal\depcalc\Event\InvalidateDepcalcCacheEvent;
use Drupal\depcalc\EventSubscriber\InvalidateDepcalcCache\InvalidateDepcalcCache;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests for InvalidateDepcalcCache.
 *
 * @group depcalc
 */
class InvalidateDepcalcCacheTest extends KernelTestBase {

  use NodeCreationTrait;
  use ContentTypeCreationTrait;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $entity;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'filter',
    'depcalc',
    'node',
    'text',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig('node');
    $this->installConfig('field');
    $this->installConfig('filter');
    $this->installSchema('node', 'node_access');

    $this->installEntitySchema('node');
    $this->installEntitySchema('user');

    $this->createContentType(
          [
            'type' => 'page',
            'name' => 'Page',
          ]
      )->save();

    $this->entity = $this->createNode(
          [
            'title' => 'Landing Page 1',
            'type' => 'page',
          ]
      );
    $this->entity->save();
  }

  /**
   * Tests depcalc cache invalidations.
   */
  public function testInvalidateDepcalcCache() {
    $dependency = new DependentEntityWrapper($this->entity);
    $cache_bin = $this->container->get('cache.depcalc');
    $cache_bin->set($dependency->getUuid(), $dependency, Cache::PERMANENT, array_keys($dependency->getDependencies()));
    $cache = $cache_bin->get($this->entity->uuid());
    $this->assertNotFalse($cache);

    $event = new InvalidateDepcalcCacheEvent($this->entity);
    /**
* @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tag_invalidator
*/
    $cache_tag_invalidator = $this->container->get('cache_tags.invalidator');
    $sut = new InvalidateDepcalcCache($cache_tag_invalidator, $cache_bin);

    $sut->onInvalidateDepcalcCache($event);
    $cache = $cache_bin->get($this->entity->uuid());
    $this->assertFalse($cache);
  }

}
