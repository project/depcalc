<?php

namespace Drupal\Tests\depcalc\Kernel;

use Drupal\depcalc\DependencyCalculatorEvents;
use Drupal\depcalc\Event\CalculateHashEvent;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\node\NodeTypeInterface;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\user\Entity\Role;

/**
 * Tests for CalculateHashEventSubscriber.
 *
 * @group depcalc
 */
class CalculateHashEventTest extends KernelTestBase {

  use NodeCreationTrait;
  use ContentTypeCreationTrait;

  /**
   * The node entity.
   *
   * @var \Drupal\Node\NodeInterface
   */
  protected NodeInterface $node;

  /**
   * Node type.
   *
   * @var \Drupal\node\NodeTypeInterface
   */
  protected NodeTypeInterface $pageContentType;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'filter',
    'depcalc',
    'depcalc_test',
    'node',
    'text',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig('node');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');

    $this->pageContentType = $this->createContentType(['type' => 'page']);
    $this->node = Node::create(
          [
            'title' => 'Testing Node',
            'type' => 'page',
          ]
      );
    $this->node->save();
  }

  /**
   * Tests removal of field from hash calculation - Content Entity.
   */
  public function testSkipHashCalculationEventForContentEntity(): void {
    $vals = $this->node->toArray();
    $original_hash = sha1(json_encode($vals));
    $this->assertArrayHasKey('title', $vals, 'Initially entity array has "title" key');
    $calculate_hash_event = new CalculateHashEvent($this->node, $vals);

    $this->container->get('event_dispatcher')->dispatch($calculate_hash_event, DependencyCalculatorEvents::HASH_CALCULATION);
    $vals = $calculate_hash_event->getHashSource();
    $new_hash = sha1(json_encode($vals));
    $this->assertArrayNotHasKey('title', $vals, 'Now entity array does not have "title" key');
    $this->assertNotSame($original_hash, $new_hash);
  }

  /**
   * Tests removal of field from hash calculation - Config Entity.
   */
  public function testSkipHashCalculationEventForConfigEntity(): void {
    $role = Role::create(['id' => 'authenticated', 'label' => 'Authenticated']);
    $role->save();

    $role_values = $role->toArray();
    $original_hash = sha1(json_encode($role_values));
    $this->assertArrayHasKey('label', $role_values, 'Initially entity array has "label" key');
    $calculate_hash_event = new CalculateHashEvent($role, $role_values);

    $this->container->get('event_dispatcher')->dispatch($calculate_hash_event, DependencyCalculatorEvents::HASH_CALCULATION);
    $role_values = $calculate_hash_event->getHashSource();
    $new_hash = sha1(json_encode($role_values));
    $this->assertArrayNotHasKey('label', $role_values, 'Now entity array does not have "label" key');
    $this->assertNotSame($original_hash, $new_hash);
  }

}
