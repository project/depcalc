<?php

namespace Drupal\depcalc\EventSubscriber\InvalidateDepcalcCache;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\depcalc\DependencyCalculatorEvents;
use Drupal\depcalc\Event\InvalidateDepcalcCacheEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Base cache invalidator.
 *
 * The basic implementation of the invalidation logic.
 * Invalidates the entity in hand and all other,
 * dependent entities. Create an event subscriber with,
 * higher priority to change the behaviour of invalidation.
 *
 * @see \Drupal\depcalc\Event\InvalidateDepcalcCacheEvent
 * @see \Drupal\depcalc\DependencyCalculatorEvents::INVALIDATE_DEPCALC_CACHE
 */
class InvalidateDepcalcCache implements EventSubscriberInterface {

  /**
   * Cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected CacheTagsInvalidatorInterface $invalidator;

  /**
   * Depcalc cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $depcalcCahe;

  /**
   * InvalidateDepcalcCache constructor.
   *
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $invalidator
   *   Cache tags invalidator service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Depcalc cache bin.
   */
  public function __construct(CacheTagsInvalidatorInterface $invalidator, CacheBackendInterface $cache) {
    $this->invalidator = $invalidator;
    $this->depcalcCahe = $cache;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    $events[DependencyCalculatorEvents::INVALIDATE_DEPCALC_CACHE][] =
      ['onInvalidateDepcalcCache', 10];
    return $events;
  }

  /**
   * Invalidates depcalc cache for the entity.
   *
   * Also starts tags invalidation.
   *
   * @param \Drupal\depcalc\Event\InvalidateDepcalcCacheEvent $event
   *   Invalidate tags event.
   */
  public function onInvalidateDepcalcCache(InvalidateDepcalcCacheEvent $event): void {
    $uuid = $event->getEntity()->uuid();
    $this->depcalcCahe->invalidate($uuid);
    $this->invalidator->invalidateTags([$uuid]);
  }

}
