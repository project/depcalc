<?php

namespace Drupal\depcalc\EventSubscriber\DependencyCollector;

use Drupal\Core\Entity\EntityInterface;
use Drupal\depcalc\DependencyCalculatorEvents;
use Drupal\depcalc\DependentEntityWrapper;
use Drupal\depcalc\Event\CalculateEntityDependenciesEvent;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;

/**
 * Calculates dependencies for Path alias.
 */
class PathAliasEntityCollector extends BaseDependencyCollector {

  /**
   * The url matcher.
   *
   * @var \Symfony\Component\Routing\Matcher\UrlMatcherInterface
   */
  protected $matcher;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[DependencyCalculatorEvents::CALCULATE_DEPENDENCIES][] = ['onCalculateDependencies'];
    return $events;
  }

  /**
   * PathAliasEntityCollector constructor.
   *
   * @param \Symfony\Component\Routing\Matcher\UrlMatcherInterface $matcher
   *   URL matcher.
   */
  public function __construct(UrlMatcherInterface $matcher) {
    $this->matcher = $matcher;
  }

  /**
   * Calculates the entities for path alias.
   *
   * @param \Drupal\depcalc\Event\CalculateEntityDependenciesEvent $event
   *   The dependency calculation event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function onCalculateDependencies(CalculateEntityDependenciesEvent $event) {
    if ($event->getEntity()->getEntityTypeId() === 'path_alias' && \Drupal::moduleHandler()->moduleExists('path_alias')) {
      /** @var \Drupal\path_alias\Entity\PathAlias $entity */
      $entity = $event->getEntity();
      $params = $this->matcher->match($entity->getPath());
      foreach ($params['_raw_variables']->keys() as $parameter) {
        if (!empty($params[$parameter]) && $params[$parameter] instanceof EntityInterface) {
          $entity_wrapper = new DependentEntityWrapper($params[$parameter]);
          $entity_wrapper->addDependency($event->getWrapper(), $event->getStack());
          $local_dependencies = [];
          $this->mergeDependencies($entity_wrapper, $event->getStack(), $this->getCalculator()
            ->calculateDependencies($entity_wrapper, $event->getStack(), $local_dependencies));
          $event->addDependency($entity_wrapper);
        }
      }
    }
  }

}
