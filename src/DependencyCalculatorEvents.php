<?php

namespace Drupal\depcalc;

/**
 * Defines events for the depcalc module.
 *
 * @see \Drupal\depcalc\Event\CalculateEntityDependenciesEvent
 */
final class DependencyCalculatorEvents {

  /**
   * Name of the event fired when an entity's dependencies are calculated.
   *
   * This event allows modules to collaborate on entity dependency calculation.
   * The event listener method receives a
   * \Drupal\depcalc\Event\CalculateEntityDependenciesEvent instance.
   *
   * @Event
   *
   * @see \Drupal\depcalc\Event\CalculateEntityDependenciesEvent
   * @see \Drupal\depcalc\DependencyCalculator::calculateDependencies
   *
   * @var string
   */
  const CALCULATE_DEPENDENCIES = "calculate_dependencies";

  /**
   * The event fired against isolated fields for dependency calculation.
   *
   * The event listener method receives a
   * \Drupal\depcalc\Event\FilterDependencyCalculationFieldsEvent instance.
   *
   * @Event
   *
   * @see \Drupal\depcalc\Event\FilterDependencyCalculationFieldsEvent
   * @see \Drupal\depcalc\FieldExtractor::getFieldsFromEntity
   */
  const FILTER_FIELDS = "depcalc_filter_fields";

  /**
   * The event fired against config entities for dependency calculation.
   *
   * The event listener method receives a
   * \Drupal\depcalc\Event\FilterDependencyConfigEntityEvent instance.
   *
   * @Event
   *
   * @see \Drupal\depcalc\Event\FilterDependencyConfigEntityEvent
   * @see \Drupal\depcalc\EventSubscriber\DependencyCollector\ConfigEntityDependencyCollector::onCalculateDependencies
   */
  const FILTER_CONFIG_ENTITIES = "depcalc_filter_config_entities";

  /**
   * Event fired when dependencies of Layout Builder Component are calculated.
   *
   * The event listener method receives a
   * \Drupal\depcalc\Event\CalculateLayoutBuilderComponentDependenciesEvent
   * instance.
   *
   * @Event
   *
   * @see \Drupal\depcalc\Event\SectionComponentDependenciesEvent
   * @see \Drupal\depcalc\EventSubscriber\DependencyCollector\LayoutBuilderFieldDependencyCollector
   *
   * @var string
   */
  const SECTION_COMPONENT_DEPENDENCIES_EVENT = "section_component_dependencies_event";

  /**
   * Name of the event fired with a dependency is invalidated from the cache.
   *
   * The event listener method recieves a
   * \Drupal\depcalc\Event\InvalidateDependenciesEvent instance.
   *
   * @Event
   *
   * @see \Drupal\depcalc\Cache\DepcalcCacheBackend
   */
  const INVALIDATE_DEPENDENCIES = "depcalc_invalidate_dependencies";

  /**
   * Name of the event fired for clearing the depcalc cache.
   *
   * This event allows module to invalidate depcalc cache of an entity along
   * with its tagged dependencies.
   *
   * @see \Drupal\depcalc\EventSubscriber\InvalidateDepcalcCache\InvalidateDepcalcCache
   */
  const INVALIDATE_DEPCALC_CACHE = "invalidate_depcalc_cache";

  /**
   * Name of the event fired to skip hash calculation.
   *
   * This event allows module to skip hash calculation
   * for specific fields of an entity.
   *
   * @var string
   */
  const HASH_CALCULATION = "hash_calculation";

}
