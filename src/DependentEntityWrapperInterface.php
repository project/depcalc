<?php

namespace Drupal\depcalc;

/**
 * Interface to collect dependency for the entity.
 */
interface DependentEntityWrapperInterface {

  /**
   * Get the entity for which we are collecting dependencies.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   An entity object. NULL if no matching entity is found.
   */
  public function getEntity();

  /**
   * The id of the entity.
   *
   * @return int|null|string
   *   The entity id.
   */
  public function getId();

  /**
   * Indicates if the entity is of type config.
   *
   * @return bool
   *   TRUE if the entity is a config entity.
   */
  public function isConfigEntity(): bool;

  /**
   * Return uuid of the entity.
   *
   * @return null|string
   *   The uuid of the entity.
   */
  public function getUuid();

  /**
   * Set remote uuid.
   *
   * @param null|string $uuid
   *   The uuid of the entity.
   */
  public function setRemoteUuid($uuid);

  /**
   * Get remote uuid.
   *
   * @return null|string
   *   The uuid.
   */
  public function getRemoteUuid();

  /**
   * Returns entity type id.
   *
   * @return string
   *   The entity type id.
   */
  public function getEntityTypeId();

  /**
   * Get the uuid/hash values of dependencies of this entity.
   *
   * @return string[]
   *   The list of uuid/hash values of dependencies of this entity.
   */
  public function getDependencies();

  /**
   * Get the uuid/hash values of direct child dependencies of this entity.
   *
   * @return string[]
   *   Array of direct child dependencies of this entity.
   */
  public function getChildDependencies(): array;

  /**
   * Document a new dependency for this entity.
   *
   * @param \Drupal\depcalc\DependentEntityWrapperInterface $dependency
   *   The dependency to add.
   * @param \Drupal\depcalc\DependencyStack $stack
   *   The dependency stack.
   * @param bool $direct_child
   *   Whether given wrapper is a direct child.
   */
  public function addDependency(DependentEntityWrapperInterface $dependency, DependencyStack $stack, bool $direct_child = TRUE);

  /**
   * Add dependencies of this entity.
   *
   * @param \Drupal\depcalc\DependencyStack $stack
   *   The dependency stack.
   * @param \Drupal\depcalc\DependentEntityWrapperInterface ...$dependencies
   *   Entities wrappers to add as a dependency.
   */
  public function addDependencies(DependencyStack $stack, DependentEntityWrapperInterface ...$dependencies);

  /**
   * Add new module dependencies.
   *
   * @param array $modules
   *   The list of modules to add as dependencies.
   */
  public function addModuleDependencies(array $modules);

  /**
   * Returns the list of module dependencies.
   *
   * @return string[]
   *   The modules this entity requires to operate.
   */
  public function getModuleDependencies();

  /**
   * The hash value of the entity.
   *
   * @return mixed
   *   The sha1 hash value of the entity.
   */
  public function getHash();

  /**
   * Whether any additional processing is needed.
   *
   * @return bool
   *   True/False.
   */
  public function needsAdditionalProcessing();

}
