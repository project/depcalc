<?php

namespace Drupal\depcalc\Cache;

use Drupal\Core\Cache\CacheFactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class to generate Depcalc cache.
 */
class DepcalcCacheFactory implements CacheFactoryInterface {

  /**
   * The core cache factory.
   *
   * @var \Drupal\Core\Cache\CacheFactoryInterface
   *   The core cache factory.
   */
  protected $factory;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   *   The event dispatcher.
   */
  protected $dispatcher;

  /**
   * DepcalcCacheFactory constructor.
   *
   * @param \Drupal\Core\Cache\CacheFactoryInterface $factory
   *   The core cache factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event dispatcher.
   */
  public function __construct(CacheFactoryInterface $factory, EventDispatcherInterface $dispatcher) {
    $this->factory = $factory;
    $this->dispatcher = $dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function get($bin) {
    $backend = $this->factory->get('depcalc');
    return new DepcalcCacheBackend($backend, $this->dispatcher);
  }

}
