<?php

namespace Drupal\depcalc\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * The event dispatched to invalidate depcalc cache for a given entity.
 *
 * @see depcalc_entity_presave
 */
class InvalidateDepcalcCacheEvent extends Event {

  /**
   * Entity for which to skip tag invalidation.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   *   Entity object.
   */
  protected EntityInterface $entity;

  /**
   * InvalidateDepcalcCacheEvent constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Returns the entity object.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Entity object.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

}
