<?php

namespace Drupal\depcalc\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * The InvalidateDependenciesEvent event.
 */
class InvalidateDependenciesEvent extends Event {

  /**
   * The list of DependentEntityWrappers being invalidated.
   *
   * @var \Drupal\depcalc\DependentEntityWrapperInterface[]
   *   The dependencies.
   */
  protected $wrappers;

  /**
   * InvalidateDependenciesEvent constructor.
   *
   * @param \Drupal\depcalc\DependentEntityWrapperInterface[] $wrappers
   *   The dependencies.
   */
  public function __construct(array $wrappers) {
    $this->wrappers = $wrappers;
  }

  /**
   * Returns the list of DependentEntityWrappers being invalidated.
   *
   * @return \Drupal\depcalc\DependentEntityWrapperInterface[]
   *   The dependencies.
   */
  public function getWrappers(): array {
    return $this->wrappers;
  }

}
