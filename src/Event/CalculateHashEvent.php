<?php

namespace Drupal\depcalc\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * The event dispatched to unset field from entity.
 *
 * @see depcalc_entity_presave
 */
class CalculateHashEvent extends Event {

  /**
   * Normalized array of the entity.
   *
   * @var array
   */
  protected array $vals = [];

  /**
   * Unmodified normalized array of the entity.
   *
   * @var array
   */
  private array $originalValues;

  /**
   * The entity in hand.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $entity;

  /**
   * CalculateHashEvent constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity in hand.
   * @param array $vals
   *   Normalized array of the entity.
   */
  public function __construct(EntityInterface $entity, array $vals) {
    $this->entity = $entity;
    $this->originalValues = $vals;
    $this->vals = $vals;
  }

  /**
   * Returns original hash source for entity.
   *
   * @return array
   *   The original entity array used for calculating hash.
   */
  public function getOriginalHashSource(): array {
    return $this->originalValues;
  }

  /**
   * Return Hash source for entity.
   *
   * @return array
   *   The entity array used for calculating hash.
   */
  public function getHashSource(): array {
    return $this->vals;
  }

  /**
   * Set Hash source for entity.
   *
   * @param array $vals
   *   Normalized array of the entity.
   */
  public function setHashSource(array $vals): void {
    $this->vals = $vals;
  }

  /**
   * Return entity in hand.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity in hand.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

}
